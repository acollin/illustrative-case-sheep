This project is the numerical implementation of a simple configuration that can be seen as an illustrative example of the impact on the electrical field of the pulsed field ablation (PFA) of a cardiac tissue near the fibrotic valvular plane. For more informations on the mathematical theory beside, read the PDF document placed in this folder (NumericalDetails.pdf). For more informations about the clinical context and to cite this work, see [1].

Inside the folder `src` you will find the code and the mesh. 

The strategy is based on the FEM, implemented in the FreeFem++ software. Version 4.9 has been used for these tests. The code has not been tested with later versions.

All Copyrights (c) of the code and the mesh belong to: Annabelle Collin, 2024, Nantes Université, CNRS, Inria. France.

`LinearModel.edp` solves both cases: (1) the catheter is in the vicinity of the fibrotic valvular plane and (2) the catheter is located far away the fibrotic valvular plane. 

`hisbundle.mesh` contains a volume mesh corresponding to the geometry. It is used in the code `LinearModel.edp`.

[1] Atrio-ventricular block during ventricular pulsed field ablation: two preclinical cases and numerical considerations. In preparation.